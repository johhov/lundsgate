#include "ScreenObject.h"

ScreenObject::~ScreenObject()
{
	SDL_FreeSurface(sprite);
}

glm::vec2 ScreenObject::getVPos()
{
	return vPos;
}

glm::vec2 ScreenObject::getSPos()
{
	return sPos;
}

glm::vec2 ScreenObject::getPosition() 
{
	return position;
}

SDL_Surface* ScreenObject::getSprite()
{
	return sprite;
}

SDL_Rect ScreenObject::getSourceRect() 
{
	SDL_Rect rect;

	rect.x = static_cast<int>(spritePos.x);
	rect.y = static_cast<int>(spritePos.y);
	rect.w = static_cast<int>(size.x);
	rect.h = static_cast<int>(size.y);
	
	return rect;
}

SDL_Rect ScreenObject::getDestRect() 
{
	SDL_Rect rect;

	rect.x = static_cast<int>(position.x);
	rect.y = static_cast<int>(position.y);
	rect.w = static_cast<int>(size.x);
	rect.h = static_cast<int>(size.y);

	return rect;
}

void ScreenObject::setPosition(glm::vec2 newPos)
{
	position = newPos;
}