#pragma	once

#include <string>
#include <vector>
#include <glm/glm.hpp>
#include "WindowHandler.h"
#include "ghost.h"
#include "TextHandler.h"
#include "InputHandler.h"
#include "GameEvent.h"
#include "Pacman.h"
#include <SDL2/SDL_mixer.h>

class Level 
{
public:
	Level(std::string levelFilepath, int lvl, Pacman& pacman);
	~Level();
	void update(float dt);

	void setUp();
	void resetLevel();
	
	uint getLevelNumber();

	std::vector<ScreenObject*>* getWalls();
	std::vector<ScreenObject*>* getOrbs();
	std::vector<ScreenObject*>* getFruits();
	std::vector<ScreenObject*>* getBigOrbs();
	std::vector<ScreenObject*>* getGhosts();
	std::vector<ScreenObject*>* getText();
	std::vector<ScreenObject*>* getPacman();

	void setPacRequestedDirection(glm::vec2 requestedDirection);

private:
	uint level;
	glm::vec2 screenSize;
	glm::vec2 tileSize;

	glm::vec2 pacmanStartPosition;
	std::string levelName;

	
	std::vector<std::vector<int>> map;
	std::vector<ScreenObject*> walls;
	std::vector<ScreenObject*> orbs;
	std::vector<ScreenObject*> fruits;
	std::vector<ScreenObject*> bigOrbs;
	std::vector<ScreenObject*> ghosts;
	std::vector<ScreenObject*> text;
	std::vector<ScreenObject*> pacman;

	int score = 0;
	
	void findLegalDirs(const glm::ivec2& higherMapPosition, glm::ivec2 dir, std::vector<glm::ivec2>& legalDirs, const bool& omniDirectional);
	void makeText();
	void handleWrapping(bool& wrapping, glm::vec2& newPosition);
	void pacmanActions(const float dt);
	void ghostAi(const float dt, Ghost* ghost);
	void isNom(const float dt);
	bool isSuper();
	void eatStuff(const int& a, const int& b, const int& s);
	void getHit(const int i);

	Mix_Music* music = nullptr;
	Mix_Chunk* waka = nullptr;
	Mix_Chunk* powerup = nullptr;
	Mix_Chunk* damage = nullptr;

	void loadMap(std::string filepath); //Loads map data from file
	SDL_Surface* loadMedia(std::string filename, bool levelBased); //Loads an image and decides if it's level based or static.
	void createObjects();	//Creates all objects
};