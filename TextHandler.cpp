#include "TextHandler.h"

TextHandler::TextHandler(const std::string txt, const glm::vec2 startPos, std::vector<ScreenObject*>* txtVector)
{
	text = txt;
	position = startPos;
	textVector = txtVector;

	font = IMG_Load("Arial1.png");

	if (font == NULL)
	{
		printf("Unable to load image %s! SDL Error: %s\n", "Arial1.png", SDL_GetError());
	}

	fontSize = glm::vec2(font->w / 20, font->h / 10);
	letterOffset = 21;

	makeText();
}

void TextHandler::makeText()
{
	for (size_t i = 0; i < text.length(); ++i)
	{
		glm::vec2 pain;
		pain.x = 0;
		pain.y = 0;
		SDL_Surface* letter = IMG_Load("black.png");

		if (font == NULL)
		{
			printf("Unable to load image %s! SDL Error: %s\n", "Arial1.png", SDL_GetError());
		}

		int letterValue = (text.at(i) - letterOffset + 8);

		glm::vec2 imagePosition = glm::vec2(letterValue * 21 % font->w, ((letterValue * 21) / font->w) * 25 - 25);

		SDL_Rect src;
		src.x = imagePosition.x;
		src.y = imagePosition.y;
		src.w = fontSize.x;
		src.h = fontSize.y;

		SDL_Rect dst;
		dst.x = 0;
		dst.y = 0;
		dst.w = fontSize.x;
		dst.h = fontSize.y;

		SDL_BlitSurface(font, &src, letter, &dst);

		textVector->emplace_back(new ScreenObject(position, fontSize, letter, pain));

		position.x += fontSize.x;
	}

	SDL_FreeSurface(font);
}