# Assignment 1

## Group creation deadline 2016/9/30 23:59:59
## Hand in deadline 2016/10/7 23:59:59

In this assignment you will be making Pac Man.  
This is a group assignment. You will make groups of 3 students (if for some reason this is not possible contact me before the group creation deadline). One of the group members must send an e-mail to me at johannes.hovland2@ntnu.no with the name of the group members by the group creation deadline.

One of the group members will have to **fork**(not clone) this repo. You will be developing on that fork.  
Remember to give Simon and me access so that we can look at what you have done.

## Required work
1. Finish the code in ```Level::createWalls()``` and ```WindowHandler::draw(ScreenObject* object)```so that walls are added to the level and can be drawn. The walls do not need to have any fancy textures. Filling an appropriate space with color will do.
2. Create a player object.
    1. Use the ```InputHandler``` to read input from **w, a, s, d** and add movement events to the event queue.
    1. Pop events from the event queue in the update function (_main.cpp_) and use them to move Pac Man around.
    1. Implement collision detection so that Pac Man cannot pass through the walls and can pick up orbs.
    1. Load the provided sprite sheet and animate Pac Man as he moves
5. Create and display objects representing orbs/crates/fruit that can be collected by Pac Man and gives him points. (Sprites/textures optional but it must be visible.) These objects should disapear as Pac Man collects them.
6. Modify the mapfile so that it includes data about where spheres that give points should be placed.
5. Create a text handling class.
    1. Use the text handling class to load the font provided.
    1. Use the font to display a score in the top left corner of the screen as Pac Man picks up orbs.
1. Update this document. (See botom.)

## Restrictions
1. No drawing to screen outside of the ```WindowHandler``` (You are free to modify it as much as you want.)
2. You **must** use the SDL2 library for the graphics.
3. Do not use SDL_TTF for the font handling.

## Suggestions for additional work
1. Add enemies (Colliding with the enemies resets Pac Man to the start position.)
    1. Give the enemies a simple AI.
2. Add functionality that let Pac Man exit on one side of the screen and enter on the other.
3. Add sound.
4. Add more levels and a way to switch between them.
5. Implement proper kerning in the font.
6. Etc

##Group comments
###Who are the members of the group?###
Odd-Kjetil Aamot Dahl

Hans Emil Eid

H�kon Kleppe Normann

###What we made (individually)###

H�kon: I added the initial collision function, aswell as working on the draw function together with Odd.Me and Odd later made a sleeker and more efficient version of the collision fuction, and with Odds version being the more efficient one we decided to go with that. I also made the "AI" for the "ghosts" where Mario will go slowly towards the player, sonic will run away, and egmman/luigi run in random directions. I made the soundeffects aswell, though that was a bit rushed towards the end.

Odd-Kjetil Aamot Dahl:
Made objects appear on screen using vectors of screenobject pointers in level sent to windowhandler.
Worked on collision for ghosts, they change direction at walls, some randomly, some with purpose.
Worked on collision for pacman, he stops when he hits a wall, and can wrap all directions. He can not turn 180 degrees, unless he is super (Disclaimer... he can't wrap UP if he's super)
Texthandler takes in a string and the vector of screenobject pointers from level, then deletes itself after the work is done.
Multiple levels are added by adding levels to the level list, and level looping works, so does game over.

Hans Emil: I worked for a long time on collision, which ended up being inferior to the code we ended up using, so that as a fun waste of time. I also did hit detection between ghosts and pacman, which uses the fantastic getIntersection command SDL has. The supermode was my pride. Initiating it is based on the rest of the eating stuff functions (which I also did, and which removes orbs, big orbs and fruits from the appropriate vectors), and I use a simple timer system for how long you are super before reverting to normal speed and sprite. Speaking of, I also did the animation for pac-man. Finally, I did general bugtesting. I also made a menu class, but didn't have time to implement it, and due to various merge conflicts I didn't have time to push it. Techinically has everything it's supposed to though.

###What parts if any of the base code did you change and why?###
###What was the hardest part of this assignment? (Individually)###
Håkon: The biggest challenge for me was making the initial collision function and reworking it.
	also stupid bugs and memory leaks that made absolutely no sense

Hans Emil: Collision, 100%. Fuck that shit.

Odd-Kjetil Aamot Dahl:
Clearing memory for destructor in level, since it somehow called itself multiple times. Was never solved, so memory leaks exist, sadly.

###Did you feel like the assignment was an appropriate ammount of work? (Individually)###
Håkon: Yes, though it left me with very little time to work on other assignments.

Hans Emil: Yes, but I could've worked more spread out rather than rush it now

Odd-Kjetil Aamot Dahl:
It was an ok amount.