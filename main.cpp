#include <stdio.h>
#include <SDL2/SDL.h>
#include <string>

#include "InputHandler.h"
#include "WindowHandler.h"
#include "globals.h"
#include "GameEvent.h"
#include "ScreenObject.h"
#include "Level.h"
#include "Pacman.h"

#include <iostream>

//Creates a Level object for each line in gLEVELS_FILE andplace it it the gLevels vector.
//Those lines are paths of files with map data.
//See Level::loadMap for more information.
//Returns a pointer to the first Level object (currentLevel).
Level* loadLevels(Pacman& pacman) 
{
	FILE* file = fopen(gLEVELS_FILE, "r");

	int f;
	std::string tempString;

	fscanf(file, "Number of files: %d", &f);

	for(int i = 1; i<=f; i++) 
	{
		char tempCString[51];
		fscanf(file, "%50s", &tempCString);
		tempString = tempCString;
		Level level(tempString, i, pacman);
		gLevels.emplace_back(level);
	}

	fclose(file);
	file = nullptr;
	
	return &gLevels.front();
}

bool levelComplete(Level* currentLevel)
{
	if (currentLevel->getOrbs()->empty() && 
		currentLevel->getBigOrbs()->empty() && 
		currentLevel->getFruits()->empty())
	{
		std::cout << "Level complete!\n";
		return true;
	}

	return false;
}

//Initializes the InputHandler and WindowHandler
//The WindowHandler intitilizes SDL
//Loads levels and returns a pointer to the first Level by calling loadLevels()
Level* init(Pacman& pacman) 
{

	Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, 2, 4096);
	Mix_AllocateChannels(4);
	
	if (firstTimeEver)
	{
		firstTimeEver = false;

		InputHandler::getInstance().init();

		if(!WindowHandler::getInstance().init()) 
		{
			gRunning = false;
		}
	}

	return loadLevels(pacman);
}

//While there are events in the eventQueue. Process those events. 
void update(float deltaTime, std::queue<GameEvent>& eventQueue, Level* currentLevel) 
{
	GameEvent nextEvent;
	while(!eventQueue.empty()) 
	{
		nextEvent = eventQueue.front();
		eventQueue.pop();

		if(nextEvent.action == ActionEnum::PLAYER_MOVE_UP)
		{
			currentLevel->setPacRequestedDirection(glm::vec2(0, -1));
		}
		else if(nextEvent.action == ActionEnum::PLAYER_MOVE_DOWN)
		{
			currentLevel->setPacRequestedDirection(glm::vec2(0, 1));
		}
		else if(nextEvent.action == ActionEnum::PLAYER_MOVE_LEFT)
		{
			currentLevel->setPacRequestedDirection(glm::vec2(-1, 0));
		}
		else if(nextEvent.action == ActionEnum::PLAYER_MOVE_RIGHT)
		{
			currentLevel->setPacRequestedDirection(glm::vec2(1, 0));
		}
	}
}

//This is the main draw function of the program. All calls to the WindowHandler for drawing purposes should originate here.
void draw(Level* currentLevel) 
{
	WindowHandler::getInstance().clear();
	
	WindowHandler::getInstance().drawList(currentLevel->getWalls());
	WindowHandler::getInstance().drawList(currentLevel->getOrbs());
	WindowHandler::getInstance().drawList(currentLevel->getFruits());
	WindowHandler::getInstance().drawList(currentLevel->getBigOrbs());
	WindowHandler::getInstance().drawList(currentLevel->getGhosts());
	WindowHandler::getInstance().drawList(currentLevel->getText());
	WindowHandler::getInstance().drawList(currentLevel->getPacman());

	WindowHandler::getInstance().update();
}

//Calls cleanup code on program exit.
void close() 
{
	WindowHandler::getInstance().close();
}

int main(int argc, char *argv[]) 
{
	std::queue<GameEvent> eventQueue; //Main event queue for the program.
	Level* currentLevel = nullptr;
	float nextFrame = 1/gFpsGoal; //Time between frames in seconds
	float nextFrameTimer = 0.0f; //Time from last frame in seconds
	float deltaTime = 0.0f; //Time since last pass through of the game loop.
	auto clockStart = std::chrono::high_resolution_clock::now(); //Clock used for timing purposes
	auto clockStop = clockStart;

	Pacman pacman;

	currentLevel = init(pacman);
	currentLevel->setUp();

	while(gRunning) 
	{

		clockStart = std::chrono::high_resolution_clock::now();

		InputHandler::getInstance().readInput(eventQueue);
		update(deltaTime, eventQueue, currentLevel);


		if(nextFrameTimer >= nextFrame) 
		{
			currentLevel->update(deltaTime);

			if (levelComplete(currentLevel) &&
				currentLevel->getLevelNumber() == gLevels.size())
			{
				gLevels.clear();

				currentLevel = init(pacman);
				currentLevel->setUp();

				pacman.reset();
			}

			if (pacman.getLives() == 0)
			{
				gLevels.clear();

				currentLevel = init(pacman);
				currentLevel->setUp();

				pacman.reset();
			}

			draw(currentLevel);
			nextFrameTimer = 0.0f;
		}

		clockStop = std::chrono::high_resolution_clock::now();
		deltaTime = std::chrono::duration<float, std::chrono::seconds::period>(clockStop - clockStart).count();
		nextFrameTimer += deltaTime;
	}

	close();
	return 0;
}