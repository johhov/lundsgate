#include "Pacman.h"

Pacman::Pacman() 
{ 
	speed = 150; 
	time = 0;
	score = 0;
	lives = 3;
	super = false;
	timer = 0;

}	

int Pacman::getScore()
{
	return score;
}

void Pacman::gainScore(int s)
{
	
	switch (s)
	{
		case 0:
			score += 20;
			break;
		case 3:
			score += 1000;
			break;
		case 4:
			score += 1500;
			super = true;
			timer = 0;
			speed = 250;
			break;
	}
}

int Pacman::getLives()
{
	return lives;
}

void Pacman::gainLives()
{
	lives++;
}

bool Pacman::superMan(float dt)
{
	if (super)
	{
		timer += dt;
		if (timer >= 6)
		{
			super = false;
			speed = 150;
			return false;
		}
		return true;
	}
	else
	{
		return false;
	}
}


void Pacman::update(float dt)
{
	
	time += dt*(speed/5);
	glm::vec2 tRect;

	if (dir == glm::ivec2(0,-1))
	{
		tRect.y = 30;
	}
	if (dir == glm::ivec2(0,1))
	{
		tRect.y = 0;
	}
	if (dir == glm::ivec2(-1,0))
	{
		tRect.y = 60;
	}
	if (dir == glm::ivec2(1,0))
	{
		tRect.y = 90;
	}



	switch ((int)time % 6)
	{
		case 0:
			tRect.x = 0;
		break;
		case 1:
			tRect.x = 30;
		break;
		case 2:
			tRect.x = 60;
		break;
		case 3:
			tRect.x = 90;
		break;
		case 4:
			tRect.x = 60;
		break;
		case 5:
			tRect.x = 30;
		break;
	}
	spritePos = tRect;
	if (time >= 6)
	{
		time = 0;	//To avoid time counting up to infinity
	}

}


float Pacman::getSpeed()
{
	return speed;
}

glm::ivec2 Pacman::getDirection()
{
	return dir;
}

glm::ivec2 Pacman::getRequestedDirection()
{
	return requestedDir;
}

bool Pacman::talkShit()
{				//Talk shit
	if (super)
	{	
		score += 2000;
		return false;
	}
	else
	{			//Get hit
		lives--;
		return true;
	}
}

void Pacman::setSprite(glm::vec2 sz, SDL_Surface* spr)
{
	size = sz;
	sprite = spr;
	spritePos.x = 0;
	spritePos.y = 0;

}

void Pacman::setDirection(glm::ivec2 direction)
{
	dir = direction;
}

void Pacman::setRequestedDirection(glm::ivec2 requestedDirection)
{
	requestedDir = requestedDirection;
}

void Pacman::setLegalDirs(std::vector<glm::ivec2> legalDirections)
{
	legalDirs.clear();

	for (size_t i = 0; i < legalDirections.size(); ++i)
	{
		legalDirs.push_back(legalDirections.at(i));
	}
}

std::vector<glm::ivec2> Pacman::getLegalDirs()
{
	return legalDirs;
}

void Pacman::reset()
{
	speed = 150; 
	time = 0;
	score = 0;
	lives = 3;
	super = false;
	timer = 0;
}