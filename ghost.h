#pragma once
#include "ScreenObject.h"



class Ghost : public ScreenObject 
{
public:
	Ghost(){};

	enum typeOfGhost
	{mario, sonic, luigi, eggman};


	Ghost(glm::vec2 pos, glm::vec2 sz, SDL_Surface* spr, int tp, glm::vec2 sprPos);
	void update(float dt);

	float getSpeed();
	glm::ivec2 getDirection();
	void setDirection(glm::ivec2 newDir);
	int getType();

private:
	
	glm::ivec2 direction;
	int type;
	float speed;
};
