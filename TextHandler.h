#include <iostream>
#include "ScreenObject.h"

class TextHandler
{
public:
	TextHandler(const std::string txt, const glm::vec2 startPos, std::vector<ScreenObject*>* txtVector);

private:
	int letterOffset;      //The first letter of the image is "!", char 21

	glm::vec2 fontSize;
	glm::vec2 position;
	std::vector<ScreenObject*>* textVector;
	std::string text;
	SDL_Surface* font = NULL;

	void makeText();
};