#include "ghost.h"

#include <iostream>

Ghost::Ghost(glm::vec2 pos, glm::vec2 sz, SDL_Surface* spr, int tp, glm::vec2 sprPos) : ScreenObject(pos, sz, spr, sprPos)			
{
	type = tp;

	direction.x = -1;
	direction.y = 0;

	switch(type)
	{
	case mario: speed = 2 * 30; break;
	case sonic: speed = 7 * 30; break;
	case luigi: speed = 3 * 30; break;
	case eggman: speed = 3 * 30; break;
	}
}

float Ghost::getSpeed()
{
	return speed;
}

glm::ivec2 Ghost::getDirection()
{
	return direction;
}

int Ghost::getType()
{
	return type;
}
void Ghost::setDirection(glm::ivec2 newDir)
{
	direction = newDir;
}

void Ghost::update(float dt)
{
	position.x += direction.x * speed;
	position.y += direction.y * speed;
}
