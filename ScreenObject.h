#pragma	once

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <vector>
#include <glm/glm.hpp>

class ScreenObject 
{
public:
	ScreenObject() {};
	ScreenObject(glm::vec2 pos, glm::vec2 sz, SDL_Surface* spr, glm::vec2 sprPos) : position(pos), sPos(pos), size(sz), sprite(spr), spritePos(sprPos) {vPos = glm::vec2(0,0);};
	ScreenObject(glm::vec2 pos, glm::vec2 sz, SDL_Surface* spr, glm::vec2 sprPos, glm::vec2 vP) : position(pos), sPos(pos), size(sz), sprite(spr), spritePos(sprPos), vPos(vP) {};
	~ScreenObject();
	//virtual void update(){};

	glm::vec2 getVPos();
	glm::vec2 getSPos();
	glm::vec2 getPosition();
	SDL_Surface* getSprite();
	SDL_Rect getSourceRect();
	SDL_Rect getDestRect();
	void setPosition(glm::vec2 newPos);


protected:
	glm::vec2 position, sPos;
	glm::vec2 size;
	SDL_Surface* sprite;
	glm::vec2 spritePos;
	glm::vec2 vPos;
};