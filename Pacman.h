#pragma once
#include <stdio.h>
#include <iostream>
#include <vector>
#include <SDL2/SDL.h>
#include <string>
#include "ScreenObject.h"
#include <SDL2/SDL_mixer.h>

class Pacman : public ScreenObject
{
public:
	Pacman();
	~Pacman() {};

	int getScore();
	void gainScore(int s);
	int getLives();
	void gainLives();

	void update(float dt);

	float getSpeed();
	glm::ivec2 getDirection();
	glm::ivec2 getRequestedDirection();
	std::vector<glm::ivec2> getLegalDirs();

	bool superMan(float dt);
	
	bool talkShit();
	void setSprite(glm::vec2 sz, SDL_Surface* spr);
	void setDirection(glm::ivec2 direction);
	void setRequestedDirection(glm::ivec2 requestedDirection);
	void setLegalDirs(std::vector<glm::ivec2> legalDirections);
	void reset();

	
private:

	int score, lives;
	bool super;
	float speed, time, timer;

	glm::ivec2 dir, requestedDir, currentlyFacing, sPos;

	std::vector<glm::ivec2> legalDirs;


};