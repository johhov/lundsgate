#include "Level.h"


Level::Level(std::string levelFilepath, int lvl, Pacman& pac) 
{
	level = lvl;
	loadMap(levelFilepath);
	tileSize.x = WindowHandler::getInstance().getScreenSize().x / map[0].size();
	tileSize.y = WindowHandler::getInstance().getScreenSize().y / map.size();
	createObjects();

	levelName = levelFilepath;

	Pacman* pacPtr= &pac;

	music = Mix_LoadMUS("music.ogg");		//loads soundfiles
	waka = Mix_LoadWAV("waka.ogg");
	powerup = Mix_LoadWAV("powerup.ogg");
	damage = Mix_LoadWAV("damage.ogg");

	pacman.push_back(pacPtr);
}

Level::~Level()
{
/*
	std::cout << walls.size() << "\n";

	for (int i = 0; i < walls.size() && walls.size() < 500; ++i)
	{
		//delete (walls.at(i));
	}

	std::cout << orbs.size() << "\n";

	for (int i = 0; i < orbs.size() && walls.size() < 250; ++i)
	{
		//delete (orbs.at(i));
	}

	std::cout << fruits.size() << "\n";

	for (int i = 0; i < fruits.size(); ++i)
	{
		//delete (fruits.at(i));
	}

	std::cout << bigOrbs.size() << "\n";

	for (int i = 0; i < bigOrbs.size(); ++i)
	{
		//delete (bigOrbs.at(i));
	}

	std::cout << ghosts.size() << "\n";

	for (int i = 0; i < ghosts.size(); ++i)
	{
		//delete (ghosts.at(i));
	}

	std::cout << text.size() << "\n";

	for (int i = 0; i < text.size(); ++i)
	{
		//delete (text.at(i));
	}

	std::cout << text.size() << "done?\n";
*/
}

void Level::loadMap(std::string levelFilepath) 
{
	int x, y, temp;
	FILE* file = fopen(levelFilepath.c_str(), "r");
	
	fscanf(file, "%dx%d", &x, &y);

	for(int i = 0; i<y; i++) 
	{
		std::vector<int> row;
		for(int j = 0; j<x; j++) 
		{
			fscanf(file, " %d", &temp);
			row.push_back(temp);
		}
		map.push_back(row);
	}

	fclose(file);
	file = nullptr;
}

SDL_Surface* Level::loadMedia(std::string filename, bool levelBased)
{
	std::string baseFilename = filename;

	if(levelBased)
	{
		baseFilename.append(std::to_string(level));
	}
	
	baseFilename.append(".png");

	SDL_Surface* sprite = IMG_Load(baseFilename.c_str());

	if (sprite == NULL)
	{
		printf("Unable to load image %s! SDL Error: %s\n", baseFilename.c_str(), SDL_GetError());
	}

	return sprite;
}

//Uses the map data to create ScreenObjects that represent the walls of the map.
void Level::createObjects() 
{
	glm::vec2 pos;
	int rows = map.size();
	int cells = 0;
	glm::vec2 pain;
	pain.x = 0;
	pain.y = 0;

	for(int i = 0; i<rows; i++) 
	{
		cells = map[i].size();

		for(int j = 0; j<cells; j++) 
		{
			if(map[i][j] == 0)
			{
				pos = glm::vec2(j * tileSize.x, i * tileSize.y);

				orbs.emplace_back(new ScreenObject(pos, tileSize, loadMedia("orb", 0), pain, glm::vec2(i,j)));
			}
			else if(map[i][j] == 1) 
			{
				pos = glm::vec2(j * tileSize.x, i * tileSize.y);

				walls.emplace_back(new ScreenObject(pos, tileSize, loadMedia("wall", 1), pain));
			}
			else if(map[i][j] == 2) 
			{
				pacmanStartPosition = glm::vec2(j * tileSize.x, i * tileSize.y);
			}
			else if(map[i][j] == 3)
			{
				pos = glm::vec2(j * tileSize.x, i * tileSize.y);

				fruits.emplace_back(new ScreenObject(pos, tileSize, loadMedia("fruit", 1), pain, glm::vec2(i,j)));
			}
			else if(map[i][j] == 4)
			{
				pos = glm::vec2(j * tileSize.x, i * tileSize.y);

				bigOrbs.emplace_back(new ScreenObject(pos, tileSize, loadMedia("bigOrb", 0), pain, glm::vec2(i,j)));
			}
			else if(map[i][j] == 5)
			{
				std::string ghostName = "ghost";

				pos = glm::vec2(j * tileSize.x, i * tileSize.y);

				ghosts.emplace_back(new Ghost(pos, tileSize, loadMedia(ghostName.append(std::to_string(ghosts.size())), 0), ghosts.size(), pain));
			}
		}
	}
}

void Level::setUp()
{
	Pacman* pac = static_cast<Pacman*>(pacman.at(0));

	glm::vec2 mapPos = pacmanStartPosition / tileSize;

	pac->setDirection(glm::ivec2(0, 0));
	pac->setPosition(pacmanStartPosition);
	pac->setSprite(tileSize, loadMedia("badpac", 0));

	std::vector<glm::ivec2> legalDirs;

	glm::ivec2 dir = glm::ivec2(1, 0);

	//Figures out pacmans first options.
	if (map[mapPos.y + dir.y][mapPos.x + dir.x] != 1)
	{
		legalDirs.push_back(dir);
	}

	dir = glm::ivec2(-1, 0);

	if (map[mapPos.y + dir.y][mapPos.x + dir.x] != 1)
	{
		legalDirs.push_back(dir);
	}

	dir = glm::ivec2(0, -1);

	if (map[mapPos.y + dir.y][mapPos.x + dir.x] != 1)
	{
		legalDirs.push_back(dir);
	}

	dir = glm::ivec2(0, 1);

	if (map[mapPos.y + dir.y][mapPos.x + dir.x] != 1)
	{
		legalDirs.push_back(dir);
	}

	pac->setLegalDirs(legalDirs);
}

void Level::setPacRequestedDirection(glm::vec2 requestedDirection)
{
	Pacman* pac = static_cast<Pacman*>(pacman.at(0));

	pac->setRequestedDirection(requestedDirection);
}

void Level::makeText()
{
	//Making sure that the surfaces of text are cleared as it will keep changing images.
	for (size_t i = 0; i < text.size(); ++i)
	{
		delete (text.at(i));
	}
	text.clear();

	Pacman* pac = static_cast<Pacman*>(pacman.at(0));

	std::string scoreString = "Score:";
	scoreString.append(std::to_string(pac->getScore()));

	TextHandler scoreText(scoreString, glm::vec2(20, 15), &text);

	std::string levelString = "Level:";
	levelString.append(std::to_string(level));

	TextHandler levelText(levelString, glm::vec2(650, 15), &text);

	std::string lifeString = "Lives: ";
	lifeString.append(std::to_string(pac->getLives()));

	TextHandler lifeText(lifeString, glm::vec2(900, 15), &text);
}

void Level::update(const float dt)
{
	if (static_cast<Pacman*>(pacman.at(0))->superMan(0))	//if superpower
	{
		Mix_PauseMusic();			//stop background music		

		if (!Mix_Playing(3)) //make sure it's not overlapping itself
		{	
			Mix_PlayChannel( 3, powerup, 0);	//play the powerup music
		}
	}
	else
	{
		Mix_HaltChannel(3);		//stops powerup sound
		Mix_ResumeMusic();		//and resume the music
	}

	if (Mix_PlayingMusic() == 0)	//plays and loops music
	{
		Mix_PlayMusic(music, 2);
	}

	uint8_t pan = (pacman.at(0)->getPosition().x /1060 * 254);
	Mix_SetPanning(1, 254 - pan,pan);
				//pans the waka according to pacmans position on x axis

	for(int i = 0; i < static_cast<int>(ghosts.size()); i++) 
	{
		Ghost* tempGO = static_cast<Ghost*>(ghosts.at(i));
		ghostAi(dt, tempGO);
	}


	isNom(dt);

	pacmanActions(dt);

	makeText();
}

void Level::findLegalDirs(const glm::ivec2& higherMapPosition, glm::ivec2 dir, std::vector<glm::ivec2>& legalDirs, const bool& omniDirectional)
{
	if (map[(higherMapPosition.y + dir.y) % map.size()][(higherMapPosition.x + dir.x) % map[0].size()] != 1)
	{
		legalDirs.push_back(dir);
	}

	//Simulates a 90 degree spin.
	glm::ivec2 tempDir = dir; dir.x = dir.y, dir.y = tempDir.x;

	if (map[(higherMapPosition.y + dir.y) % map.size()][(higherMapPosition.x + dir.x) % map[0].size()] != 1)
	{
		legalDirs.push_back(dir);
	}

	//Then a 180 degree, after this, it will have checked all 3 available directions.
	dir *= -1;

	if (map[(higherMapPosition.y + dir.y) % map.size()][(higherMapPosition.x + dir.x) % map[0].size()] != 1)
	{
		legalDirs.push_back(dir);
	}

	//When pacman is super, he can 180 as well
	if (omniDirectional)
	{
		tempDir = dir; dir.x = dir.y, dir.y = tempDir.x;

		if (map[(higherMapPosition.y + dir.y) % map.size()][(higherMapPosition.x + dir.x) % map[0].size()] != 1)
		{
			legalDirs.push_back(dir);
		}
	}
}

void Level::handleWrapping(bool& wrapping, glm::vec2& newPosition)
{
	if (newPosition.x >= WindowHandler::getInstance().getScreenSize().x ||
		newPosition.y >= WindowHandler::getInstance().getScreenSize().y ||
		newPosition.x <  0 || newPosition.y < 0)
	{
		wrapping = true;
	}

	//Couldn't find any modulo that worked on negative floats between 0 and -1.
	if (newPosition.x >= WindowHandler::getInstance().getScreenSize().x)
	{
		newPosition.x -= WindowHandler::getInstance().getScreenSize().x;
	}
	else if (newPosition.x < 0)
	{
		newPosition.x += WindowHandler::getInstance().getScreenSize().x;
	}
	else if (newPosition.y >= WindowHandler::getInstance().getScreenSize().y)
	{
		newPosition.y -= WindowHandler::getInstance().getScreenSize().y;
	}
	else if (newPosition.y < 0)
	{
		newPosition.y += WindowHandler::getInstance().getScreenSize().y;
	}
}

void Level::pacmanActions(const float dt)
{
	bool wrapping = false;

	Pacman* pacmanPtr = static_cast<Pacman*>(pacman.at(0));

	glm::ivec2 dir = pacmanPtr->getDirection();
	glm::ivec2 requestedDir = pacmanPtr->getRequestedDirection();

	if (dir != glm::ivec2(0,0))
	{
		pacmanPtr->update(dt);
	}

	//Checks current tile position.
	glm::ivec2 currentMapPosition = pacmanPtr->getPosition();
	currentMapPosition /= tileSize;

	//Finds the exact next step.
	glm::vec2 newPosition = pacmanPtr->getPosition();
	newPosition.x += dir.x * pacmanPtr->getSpeed() * dt;
	newPosition.y += dir.y * pacmanPtr->getSpeed() * dt;

	handleWrapping(wrapping, newPosition);

	//Figures out next map tile position.
	glm::ivec2 newMapPosition = newPosition;
	newMapPosition /= tileSize;

	//If pacman collided with a wall, it would have no direction.
	if (dir == glm::ivec2(0, 0))
	{
		Mix_HaltChannel(1);	//stops the waka sound when you're standing still
		
		
		std::vector<glm::ivec2> legalDirs = pacmanPtr->getLegalDirs();

		for (size_t i = 0; i < legalDirs.size(); ++i)
		{
			if (requestedDir == legalDirs.at(i))
			{
				pacmanPtr->setDirection(requestedDir);
			}
		}
	}
	else if (currentMapPosition != newMapPosition)
	{
		glm::ivec2 higherMapPosition;

				
			if (Mix_Playing(1) == false && !static_cast<Pacman*>(pacman.at(0))->superMan(0))
			Mix_PlayChannel( 1, waka, 0 ); 	//loops the waka sound while you're moving
											// and you don't have superpower

		if (currentMapPosition.x + currentMapPosition.y > newMapPosition.x + newMapPosition.y)
		{
			if (wrapping)
			{
				higherMapPosition = newMapPosition;
			}
			else
			{
				higherMapPosition = currentMapPosition;
			}
		}
		else
		{
			if (wrapping)
			{
				higherMapPosition = currentMapPosition;
			}
			else
			{
				higherMapPosition = newMapPosition;
			}
		}

		float distanceFromCentre = abs(newPosition.x - (higherMapPosition.x * tileSize.x) +
									   newPosition.y - (higherMapPosition.y * tileSize.y)) + 0.01;
	
		std::vector<glm::ivec2> legalDirs;

		if (map[higherMapPosition.y][higherMapPosition.x] == 0	||
			map[higherMapPosition.y][higherMapPosition.x] == 3	||
			map[higherMapPosition.y][higherMapPosition.x] == 4)
		{
			int a =	higherMapPosition.y;
			int b =	higherMapPosition.x;
			int s = map[higherMapPosition.y][higherMapPosition.x];
			eatStuff(a, b, s);
		}

		findLegalDirs(higherMapPosition, dir, legalDirs, pacmanPtr->superMan(dt));

		pacmanPtr->setLegalDirs(legalDirs);

		for (size_t i = 0; i < legalDirs.size(); ++i)
		{
			if (requestedDir == legalDirs.at(i))
			{
				newPosition = glm::vec2((higherMapPosition.x * tileSize.x) + distanceFromCentre * requestedDir.x,
					 				    (higherMapPosition.y * tileSize.y) + distanceFromCentre * requestedDir.y);

				if (newPosition.x < 0)
				{
					newPosition.x *= - 1;
				}
				else if (newPosition.y < 0)
				{
					newPosition.y *= -1;
				}

				pacmanPtr->setDirection(requestedDir);
			} //If it hits a wall dead on, it stops
			else if (map[(higherMapPosition.y + dir.y) % map.size()][(higherMapPosition.x + dir.x) % map[0].size()] == 1)
			{
				newPosition = glm::vec2(higherMapPosition.x * tileSize.x,
										higherMapPosition.y * tileSize.y);

				pacmanPtr->setDirection(glm::ivec2(0, 0));
			}
		}

		pacmanPtr->setPosition(newPosition);
	}
	else
	{
		pacmanPtr->setPosition(newPosition);
	}

	//Resets requested direction so that it doesn't linger till next check.
	pacmanPtr->setRequestedDirection(glm::ivec2(0, 0));

}

void Level::isNom(const float dt)
{
	Pacman* pacptr = static_cast<Pacman*>(pacman.at(0));
	SDL_Rect pac = pacptr->getDestRect();
	SDL_Rect gho;

	if(pacptr->superMan(dt))
	{
		pacptr->setSprite(tileSize, loadMedia("superpac", 0));
	}
	else
	{
		pacptr->setSprite(tileSize, loadMedia("pacsprite", 0));
	}
	for (size_t i = 0; i < ghosts.size(); ++i)
	{
		gho = ghosts.at(i)->getDestRect();
		if (SDL_HasIntersection(&gho, &pac))
		{
			if (!pacptr->talkShit())
			{
				getHit(i);
				Mix_PlayChannel( 1, damage, 0);
					//plays wilhelm scream when you kill a ghost (not at all annoying)
			}
			else
			{	
				setUp();
				for (size_t j = 0; j < ghosts.size(); j++)
				{
					getHit(j);
				}
			}
			return;
		}
	}
}

void Level::ghostAi(const float dt, Ghost* ghost)
{
	bool wrapping = false;

	//Dir gets changes during this function.
	glm::ivec2 dir = ghost->getDirection();

	//Checks current tile position.
	glm::ivec2 currentMapPosition = ghost->getPosition();
	currentMapPosition /= tileSize;

	//Finds the exact next step.
	glm::vec2 newPosition = ghost->getPosition();

	newPosition.x += dir.x * ghost->getSpeed() * dt;
	newPosition.y += dir.y * ghost->getSpeed() * dt;

	handleWrapping(wrapping, newPosition);


	//Figures out next map tile position.
	glm::ivec2 newMapPosition = newPosition;
	newMapPosition /= tileSize;


	//If it appears that the 2 tiles are different, a transition occurs.
	if (currentMapPosition != newMapPosition)
	{
		//This variable ensures that it will retain the higher tile, so
		// position works all 4 directions.
		glm::ivec2 higherMapPosition;


		if (currentMapPosition.x + currentMapPosition.y > newMapPosition.x + newMapPosition.y)
		{
			if (wrapping)
			{
				higherMapPosition = newMapPosition;
			}
			else
			{
				higherMapPosition = currentMapPosition;
			}
		}
		else
		{
			higherMapPosition = newMapPosition;
		}


		//Figures out distance from centre to offset the new position properly
		// for a smooth transition. Should never be zero.
		float distanceFromCentre = abs(newPosition.x - (higherMapPosition.x * tileSize.x) +
									   newPosition.y - (higherMapPosition.y * tileSize.y)) + 0.01;

		std::vector<glm::ivec2> legalDirs;

		findLegalDirs(higherMapPosition, dir, legalDirs, false);

		//Chooses an available direction based on behaviour.
		int choice = rand() % legalDirs.size();
		int minDiff = 9999;
		int maxDiff = 0;

		glm::vec2 pacPos = pacman.at(0)->getPosition();

		for (size_t i = 0; i < legalDirs.size(); ++i)
		{

			int xDiff = pacPos.x - (currentMapPosition.x +  legalDirs.at(i).x)*tileSize.x;
			int yDiff = pacPos.y - (currentMapPosition.y +  legalDirs.at(i).y)*tileSize.y;
			int tempDiffInt = abs(xDiff) + abs(yDiff);
			
			if (tempDiffInt < minDiff && ghost->getType() == ghost->mario)
			{
				minDiff = tempDiffInt;
				choice = i;
			}
			if (tempDiffInt > maxDiff && ghost->getType() == ghost->sonic)
			{
				maxDiff = tempDiffInt;
				choice = i;
			}
		}

		dir = legalDirs.at(choice);
		ghost->setDirection(dir);

		//Left and up wrapping
		if (dir.x + dir.y < 0 && wrapping)
		{
				higherMapPosition -= dir;
				distanceFromCentre = abs(newPosition.x - (higherMapPosition.x * tileSize.x) +
									   newPosition.y - (higherMapPosition.y * tileSize.y));
		}

		newPosition = glm::vec2((higherMapPosition.x * tileSize.x) + distanceFromCentre * dir.x,
									 (higherMapPosition.y * tileSize.y) + distanceFromCentre * dir.y);
	}

	ghost->setPosition(newPosition);
}

void Level::eatStuff(const int& a, const int& b, const int& s)
{
	Pacman* pacmanPtr = static_cast<Pacman*>(pacman.at(0));
	std::vector<ScreenObject*> stuff;
	switch(s)
	{
		case 0:
			stuff = orbs;
			break;
		case 3:
			stuff = fruits;
			break;
		case 4:
			stuff = bigOrbs;
			break;
	}
	for (size_t i = 0; i < stuff.size(); i++)
	{
		if (stuff.at(i)->getVPos() == glm::vec2(a,b))
		{
			stuff[i] = stuff.back();
			stuff.pop_back();
		}
	}
		switch(s)
	{
		case 0:
			orbs = stuff;
			break;
		case 3:
			fruits = stuff;
			break;
		case 4:
			bigOrbs = stuff;
			break;
	}

	map[a][b] = 6;
	pacmanPtr->gainScore(s);
	
}

void Level::getHit(const int i)
{
	Ghost* ghost = static_cast<Ghost*>(ghosts.at(i));
	glm::vec2 sPos = ghost->getSPos();
	ghost->setDirection(glm::vec2(-1,0));
	ghost->setPosition(sPos);

}

uint Level::getLevelNumber()
{
	return level;
}

std::vector<ScreenObject*>* Level::getWalls() 
{
	return &walls;
}

std::vector<ScreenObject*>* Level::getOrbs() 
{
	return &orbs;
}

std::vector<ScreenObject*>* Level::getFruits() 
{
	return &fruits;
}

std::vector<ScreenObject*>* Level::getBigOrbs() 
{
	return &bigOrbs;
}

std::vector<ScreenObject*>* Level::getGhosts() 
{
	return &ghosts;
}

std::vector<ScreenObject*>* Level::getText() 
{
	return &text;
}

std::vector<ScreenObject*>* Level::getPacman()
{
	return &pacman;
}